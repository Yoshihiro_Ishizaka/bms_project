Rails.application.routes.draw do
  root "home#index"
  devise_for :users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  #get  '/' => 'home#index'

  get  'login' => 'users#login'
  post 'login' => 'users#execute_login'
  post 'logout' => 'users#logout'

  get  'users/ranking' => 'users#ranking'
  get  'users/:id' => 'users#show'
  get  'users/:id/reviews' => 'users#show_reviews'
  get  'users/:id/histories' => 'users#show_histories'

  get  'books/search' => 'books#search'
  get  'books/:id' => 'books#show'
  # 貸出手続
  get  'books/:id/lend' => 'books#lend'
  post  'books/:id/lend' => 'books#execute_lend'
  # 返却手続
  get  'books/:id/return' => 'books#return'
  post  'books/:id/return' => 'books#execute_return'
  # 予約手続
  post 'books/:id/reserve' => 'books#execute_reserve'
  delete 'books/:id/reserve' => 'books#delete_reservation'
  # 図書別感想文標示
  get  'books/:id/review' => 'books#show_review'
  # 感想文新規作成
  get  'books/:id/review/new' => 'books#new_review'
  # タグ追加
  get  'books/:id/edit_tag' => 'books#edit_tag'
  post 'books/:id/add_tag' => 'books#add_tag'
  delete 'books/:id/tag' => 'books#delete_tag'

  # temporary
  get  'books/bar-code/search' => 'books#bar_code_search'
  get  'books/bar-code/result' => 'books#bar_code_result'
  post 'books/bar-code/send' => 'books#api_send'
  post 'books' => 'books#create'
  post 'books/bar-code' => 'books#bar_code_create'
  #

  get  'reviews' => 'reviews#index'
  post 'reviews' => 'reviews#create'
  get  'reviews/:id' => 'reviews#show'
  post 'reviews/:id' => 'reviews#toggle_good'
  put  'reviews/:id' => 'reviews#update'
  get  'reviews/:id/edit' => 'reviews#edit'

  delete 'reservations/:id' => 'reservations#delete'

  post 'home/send_message' => 'home#send_message'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '*not_found' => 'application#routing_error'
  post '*not_found' => 'application#routing_error'
end
