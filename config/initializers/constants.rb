module Constants
  R_API_URL_BASE= "https://app.rakuten.co.jp/services/api/BooksBook/Search/20170404?applicationId=1051096085257389301&isbn="
  C_API_URL_BASE= "https://api.chatwork.com/v2/rooms/"
  C_API_TOKEN = "ba654fd55e3074c41e2496671b996127"
  PROXY = 'http://10.0.2.21:8080'
  PROXY_ADDR = '10.0.2.21'
  PROXY_PORT = 8080
  HASH_CONVERT_MAP = {
      'title' => :title,
      'titleKana' => :title_kana,
      'subTitle' => :sub_title,
      'subTitleKana' => :sub_title_kana,
      'author' => :author,
      'authorKana' => :author_kana,
      'seriesName' => :series_name,
      'seriesNameKana' => :series_name_kana,
      'publisherName' => :publisher_name,
      'size' => :size,
      'isbn' => :isbn,
      'itemCaption' => :caption,
      'salesDate' => :published_date,
      'largeImageUrl' => :image_url,
      'category_id' => :category_id,
      'location_id' => :location_id
    }

  LABEL_CONVERT_MAP = {
      :title => 'タイトル（必須）',
      :titleKana => 'タイトルカナ（必須）',
      :subTitle => 'サブタイトル',
      :subTitleKana => 'サブタイトルカナ',
      :author => '著者名（必須）',
      :authorKana => '著者名カナ（必須）',
      :seriesName => 'シリーズ名',
      :seriesNameKana => 'シリーズ名カナ',
      :publisherName => '出版社名（必須）',
      :size => '図書サイズ',
      :isbn => 'ISBN番号（必須）',
      :itemCaption => '概要',
      :salesDate => '出版年月日',
      :largeImageUrl => '画像URL',
    }
end
