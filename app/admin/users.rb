ActiveAdmin.register User do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  before_action :check_admin

  controller do
    def check_admin
      if current_user.is_admin != 1
        flash[:alert] = "管理者のみ使用できるページです。"
        redirect_to('/')
      end
    end

    def scoped_collection
      User.with_deleted
    end

    def permitted_params
      params.permit!
    end
  end

  actions :all, except: :new
  index do
    selectable_column
    id_column
    column :name
    column (:username) {|user| user['username'] }
    column :is_admin
    column (:department_id) {|user| Department.find(user.department_id).name}
    column :chatwork_id
    column :created_at
    column :updated_at
    column :deleted_at
    column :current_sign_in_at
    column :last_sign_in_at
    column :current_sign_in_ip
    column :last_sign_in_ip
    actions
  end

  filter :is_admin, as: :select
  filter :department_id, as: :select, collection: proc { Department.all }
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range
  filter :deleted_at, as: :date_range

  form do |f|
    f.semantic_errors # shows errors on :base
    f.inputs :chatwork_id
    f.inputs except: [:username, :remember_created_at, :current_sign_in_at, :last_sign_in_at, :current_sign_in_ip, :last_sign_in_ip]
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end

end
