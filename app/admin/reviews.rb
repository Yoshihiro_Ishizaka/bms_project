ActiveAdmin.register Review do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  before_action :check_admin

  controller do
    def check_admin
      if current_user.is_admin != 1
        flash[:alert] = "管理者のみ使用できるページです。"
        redirect_to('/')
      end
    end

    def permitted_params
      params.permit!
    end
  end

  actions :all, except: :new
  index do
    selectable_column
    id_column
    column :book_id, sortable: :book_id do |review|
      Book.find(review.book_id).title
    end
    column :user_id, sortable: :user_id do |review|
      User.find(review.user_id).name
    end
    column :title
    column :text do |review|
      link_to '全文', admin_review_path(review)
    end
    column :good_count do |review|
      Good.where(review_id: review.id).count
    end
    column :isbn
    column :created_at
    column :updated_at
  end

  filter :book_id, as: :select, collection: proc { Book.all }
  filter :user_id, as: :select, collection: proc { User.all }
  filter :title, as: :string
  filter :text, as: :string
  filter :isbn, as: :string
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range

end
