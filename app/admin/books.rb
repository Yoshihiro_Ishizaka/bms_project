ActiveAdmin.register Book do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
  before_action :check_admin
  controller do
    def check_admin
      if current_user.is_admin != 1
        flash[:alert] = "管理者のみ使用できるページです。"
        redirect_to('/')
      end
    end

    def scoped_collection
      Book.with_deleted
    end

    def permitted_params
      params.permit!
    end
  end

  csv :force_quotes => false do
    Book.column_names.each do |col|
      logger.debug(col)
      if col == 'location_id'
        column ('Location') {|book| Location.find(book.location_id).name}
      elsif col == 'category_id'
        column ('Category') {|book| Category.find(book.category_id).name}
      else
        column col.to_sym
      end
    end
  end

  scope :all, default: true
  scope :available do |books|
    books.where(availability: 1)
  end
  scope :not_available do |books|
    books.where(availability: 0)
  end
  scope :waiting do |books|
    books.where(availability: 2)
  end
  scope :no_image do |books|
    books.where(image_url: nil)
  end

  index do
    selectable_column
    id_column
    column :title
    column :title_kana
    column :sub_title
    column :sub_title_kana
    column :author
    column :author_kana
    column :series_name
    column :series_name_kana
    column :publisher_name
    column :published_date
    column :caption do |book|
      link_to '全文', admin_book_path(book)
    end
    column :isbn
    column :size
    column :image_url do |book|
      unless book.image_url.blank?
        link_to '全文', admin_book_path(book)
      end
    end
    column :location_id, sortable: :location_id do |book|
      Location.find(book.location_id).name
    end
    column :category_id, sortable: :category_id do |book|
      Category.find(book.category_id).name
    end
    column :availability
    column :created_at
    column :updated_at
    column :deleted_at
    actions
  end

  filter :title, as: :string
  filter :title_kana, as: :string
  filter :author, as: :string
  filter :author_kana, as: :string
  filter :series_name, as: :select
  filter :publisher_name, as: :select
  filter :published_date, as: :string
  filter :caption, as: :string
  filter :isbn, as: :string
  filter :size, as: :select
  filter :location_id, as: :select, collection: proc { Location.all }
  filter :category_id, as: :select, collection: proc { Category.all }
  filter :availability, as: :select
  filter :created_at, as: :date_range
  filter :updated_at, as: :date_range

end
