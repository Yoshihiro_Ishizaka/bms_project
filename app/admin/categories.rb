ActiveAdmin.register Category do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  before_action :check_admin

  controller do
    def check_admin
      if current_user.is_admin != 1
        flash[:alert] = "管理者のみ使用できるページです。"
        redirect_to('/')
      end
    end

    def permitted_params
      params.permit!
    end
  end

  actions :all, :except => [:destroy, :show]
  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end

end
