ActiveAdmin.register History do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  before_action :check_admin

  controller do
    def check_admin
      if current_user.is_admin != 1
        flash[:alert] = "管理者のみ使用できるページです。"
        redirect_to('/')
      end
    end

    def permitted_params
      params.permit!
    end
  end

  scope :all, default: true
  scope :not_returned do |histories|
    histories.where(is_returned: 0)
  end
  scope :returned_but_not_checked do |histories|
    histories.where(is_returned: 1, is_checked: 0)
  end
  scope :returned_and_checked do |histories|
    histories.where(is_returned: 1, is_checked: 1)
  end
  scope :delayed do |histories|
    histories.where(is_returned: 0)
             .where('created_at < ?', eval("Time.current-#{Environment.lend_term}.day"))
  end

  actions :all, except: :new
=begin
  index do
    selectable_column
    id_column
    column :user_id
    column :book_id
    column :is_returned
    column :is_checked
    column :created_at
    column :updated_at
    actions
  end
=end

  form do |f|
    f.semantic_errors # shows errors on :base
    inputs :is_returned, :is_checked, :created_at
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end


end
