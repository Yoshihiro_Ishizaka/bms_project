module ChatworkHelper
  def send_message
    unreturned_histories = History.where(is_returned: 0)

    #return_today = undone_histories.where(created_at: Time.zone.today.beginning_of_day...Time.zone.today.end_of_day)
    #delayed_histories = undone_histories.where('created_at < ?', eval("Time.current-#{Environment.lend_term}.day"))
 
    unreturned_histories.each do |history|
      return_due = history.created_at + Environment.first.lend_term.day
      logger.debug("return_due")
      logger.debug(return_due)
      if return_due >= Time.zone.today.beginning_of_day && return_due < Time.zone.today.end_of_day
        send_request(history, :due)
      elsif return_due < Time.zone.today.beginning_of_day
        send_request(history, :urge)
      end
    end

    available_reservations = Reservation.where.not(available_at: nil)

    available_reservations.each do |reservation|
      lend_due = reservation.available_at + Environment.first.reservation_term.day
      if lend_due >= Time.zone.today.beginning_of_day && lend_due < Time.zone.today.end_of_day
        send_request(reservation, :reservation_due)
      elsif lend_due < Time.zone.today.beginning_of_day
        send_request(reservation, :cancel)
        reservation.delete
      end
    end
  end

  def send_request(instance, kind)
    user = User.find(instance.user_id)
    return false if user.chatwork_id == nil

    # ChatworkAPIへの接続設定
    uri = URI.parse(Constants::C_API_URL_BASE + user.chatwork_id + "/messages")
    proxy_addr = Constants::PROXY_ADDR
    proxy_port = Constants::PROXY_PORT
    req = Net::HTTP::Post.new(uri.path)
    req["X-ChatWorkToken"] = Constants::C_API_TOKEN
    https = Net::HTTP.new(uri.host, uri.port,proxy_addr, proxy_port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

    if kind == :due || kind == :urge
      body = generate_message_to_borrowers(instance, kind)
    elsif
      body = generate_message_to_reservers(instance, kind)
    end
    req.set_form_data({body: body})

    ## 接続を開始
    res = https.start do
      https.request(req)
    end

    # レスポンスステータスを表示
    # logger.debug(res.code)

    # レスポンスヘッダを表示
    # res.each_header do |name, val|
    #   logger.debug("#{name}: #{val}")
    # end

    # レスポンスボディを表示
    # logger.debug(res.body)
  end

  def generate_message_to_borrowers(history, kind)
    user = User.find(history.user_id)
    book = Book.find(history.book_id)
    body = "【シンクスクエア図書管理システムからのお知らせ】\n"
    if kind == :due
      body += "本日は#{user.name}さんに貸出中の\n"
      body += "『#{book.title}』の返却期限です。\n"
      body += "図書の返却を宜しくお願い致します。\n"
    else
      body += "#{user.name}さんに貸出中の\n"
      body += "『#{book.title}』の返却期限が過ぎています。\n"
      body += "図書の円滑な利用のため、早めのご返却を宜しくお願い致します。\n"
    end
    body += "貸出日: #{history.created_at}\n"
    body += "返却期限日: #{history.created_at + Environment.first.lend_term.day}\n"
    return body
  end

  def generate_message_to_reservers(reservation, kind)
    user = User.find(reservation.user_id)
    book = Book.find(reservation.book_id)
    body = "【シンクスクエア図書管理システムからのお知らせ】\n"
    if kind == :reservation_due
      body += "#{user.name}さん向けに取り置きしていた\n"
      body += "『#{book.title}』の取り置き期限が近づいております。\n"
      body += "取り置き期限日までに貸出の手続きをして下さい。\n"
    else
      body += "#{user.name}さん向けに取り置きしていた\n"
      body += "『#{book.title}』が取り置き期限を過ぎました。\n"
      body += "最初から貸出、または予約の手続きをして下さい。\n"
    end
    body += "取り置き期限日: #{reservation.available_at + Environment.first.reservation_term.day}\n"
    return body
  end

end
