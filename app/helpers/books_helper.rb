module BooksHelper
  def make_hash
    return {
      title: '',
      titleKana: '',
      subTitle: '',
      subTitleKana: '',
      author: '',
      authorKana: '',
      seriesName: '',
      seriesNameKana: '',
      publisherName: '',
      size: '',
      isbn: '',
      itemCaption: '',
      salesDate: '',
      largeImageUrl: ''
    }
  end
  
  def convert_map
    return {
      title: :title,
      titleKana: :title_kana,
      subTitle: :sub_title,
      subTitleKana: :sub_title_kana,
      author: :author,
      authorKana: :author_kana,
      seriesName: :series_name,
      seriesNameKana: :series_name_kana,
      publisherName: :publisher_name,
      size: :size,
      isbn: :isbn,
      itemCaption: :caption,
      salesDate: :published_date,
      largeImageUrl: :image_url
    }
  end

  def order_hashes(data)
    hash = make_hash
    hash.each do |k, v|
      hash[k] = data[k]
    end
    return hash
  end

  def convert_hashes(hash)
    hash.each do |k, v|
      case k
      when :title
      end
    end
  end

end
