class Environment < ApplicationRecord
  def self.lend_term
    return Environment.find(1).lend_term
  end

  def self.max_lend_count
    return Environment.find(1).max_lend_count
  end

  def self.is_require_check
    return Environment.find(1).is_require_check
  end
end
