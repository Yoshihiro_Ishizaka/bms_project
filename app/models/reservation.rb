class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :book

  def fetch_borrowing_user_info
    history = History.find_by(book_id: self.book_id, is_returned: 0)
    if history
      user_name = User.find(history.user_id).name
      lend_term = Environment.find(1).lend_term # 日
      values = { borrowed_from: history.created_at,
             return_by: history.created_at.since(lend_term.days),
             name: user_name, type: 'is_borrowed' }
    else
      reservation = Reservation.where(book_id: self.book_id).where.not(available_at: nil)
      logger.debug('Vegetable')
      logger.debug(reservation.first)
      user_name = User.find(reservation.first.user_id).name
      values = { name: user_name, type: 'is_waiting' }
    end
    return values
  end

  def self.reserve(user_id, book_id)
    if History.where(user_id: user_id).where(book_id: book_id).where(is_returned: 0).blank?
      if User.find(user_id).can_borrow?
        Reservation.create(user_id: user_id, book_id: book_id)
        return [true, nil]
      else
        return [false, :max_reached]
      end
    else
      return [false, :has_borrowed]
    end
  end

  def self.check_and_proceed_next_reservation(book_id)
    next_reservation = Reservation.where(book_id: book_id).first

    # 次の予約者の取り置き期間を開始
    if next_reservation
      next_reservation.available_at = Time.now
      next_reservation.save

      send_request(next_reservation, :ready)

      return true
    else
      # 予約されていなかったことを返す
      return false
    end
  end

  def self.is_reserved_by_user(user_id, book_id)
    reservation = Reservation.where(user_id: user_id, book_id: book_id)
    return !(reservation.blank?)
  end

  def self.send_request(instance, kind)
    user = User.find(instance.user_id)
    return false if user.chatwork_id == nil

    # ChatworkAPIへの接続設定
    uri = URI.parse(Constants::C_API_URL_BASE + user.chatwork_id + "/messages")
    proxy_addr = Constants::PROXY_ADDR
    proxy_port = Constants::PROXY_PORT
    req = Net::HTTP::Post.new(uri.path)
    req["X-ChatWorkToken"] = Constants::C_API_TOKEN
    https = Net::HTTP.new(uri.host, uri.port,proxy_addr, proxy_port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE

    if kind == :due || kind == :urge
      body = generate_message_to_borrowers(instance, kind)
    elsif kind == :ready
      body = generate_message_to_reservers(instance, kind)
    end
    req.set_form_data({body: body})

    ## 接続を開始
    res = https.start do
      https.request(req)
    end
  end

  def self.generate_message_to_reservers(reservation, kind)
    user = User.find(reservation.user_id)
    book = Book.find(reservation.book_id)
    body = "【シンクスクエア図書管理システムからのお知らせ】\n"
    if kind == :ready
      body += "#{user.name}さんが予約していた\n"
      body += "『#{book.title}』が利用可能になりました。\n"
      body += "取り置き期限日までに貸出の手続きをして下さい。\n"
    end
    body += "取り置き期限日: #{(reservation.available_at + Environment.first.reservation_term.day).to_date}\n"
    return body
  end

end
