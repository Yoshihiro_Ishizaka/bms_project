class History < ApplicationRecord
  belongs_to :user
  belongs_to :book

  def execute_return
    self.is_returned = 1
    environment = Environment.find(1)
    self.is_checked = 1 if environment.is_require_check == 0
    self.save

    # true: 次の予約者がいる false: 次の予約者がいない
    return Reservation.check_and_proceed_next_reservation(self.book_id)
  end

  private
  def self.new_lend(book_id, user_id)
    history = History.new
    logger.debug('new_lend method')
    logger.debug("#{book_id}")
    logger.debug("#{user_id}")
    history.user_id = user_id
    history.book_id = book_id
    history.save
  end

end
