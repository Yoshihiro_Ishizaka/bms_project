class Tag < ApplicationRecord
  belongs_to :user
  has_many :tag_maps
  has_many :books, through: :tag_maps

  def self.start_add_tag(user_id, book_id, tags)
    tags = tags.split(/ |　/)
    text = ""
    tags.each do |tag|
      unless tag == ""
        text += Tag.add_tag(user_id, book_id, tag)
      end
    end
    return text
  end

  def self.add_tag(user_id, book_id, name)
    if name.length > 45
      return "「#{name}」は文字数制限オーバーです。　　"
    end

    tag = Tag.find_by(name: name)
    unless tag
      tag = Tag.create(name: name, user_id: user_id)
    end

    if TagMap.is_dubbed(book_id, tag.id)
      return "入力されたタグ「#{name}」は既に登録されています。　　"
    else
      TagMap.create(tag_id: tag.id, book_id: book_id, user_id: user_id)
      return "タグ「#{name}」が追加されました。　　"
    end
  end
end
