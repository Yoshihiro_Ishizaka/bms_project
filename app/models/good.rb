class Good < ApplicationRecord
  belongs_to :user
  belongs_to :review

  def self.toggle_good(review_id, user_id)
    good = Good.find_by(review_id: review_id, user_id: user_id)
    if good.blank?
      Good.create(review_id: review_id, user_id: user_id)
    else
      good.delete
    end
  end
end
