class Book < ApplicationRecord
  belongs_to :location
  belongs_to :category
  has_many :histories
  has_many :reservations
  has_many :reviews
  has_many :tag_maps
  has_many :tags, through: :tag_maps

  acts_as_paranoid

  validates :title, presence: true
  validates :title_kana, presence: true
  validates :author, presence: true
  validates :author_kana, presence: true
  validates :publisher_name, presence: true
  validates :isbn, presence: true

  def title
    if self.deleted_at.blank?
      return self['title']
    else
      return self['title'] + "【削除済】"
    end
  end

  def lend(user_id)
    unless User.find(user_id).can_borrow?
      return false
    end
    reservation = Reservation.is_reserved_by_user(user_id, self.id)
    if reservation
      Reservation.find_by(user_id: user_id, book_id: self.id).delete
    end
    self.availability = 0
    History.new_lend(self.id, user_id)
    self.save
  end

  def execute_return
    # 一番早くなされた予約を取得
    history = History.where('book_id like ?', self.id).last

    # 次の予約者に借りられなければ(falseなら)貸出可能にする
    is_borrowed_by_next_reserver = history.execute_return
    unless is_borrowed_by_next_reserver
      self.availability = 1
    else
      self.availability = 2
    end
    self.save
  end

  def oldest_reservation_user(book_id)
    oldest_reservation_user = Reservation.where(book_id: book_id).first
    if oldest_reservation_user
      return oldest_reservation_user.user_id
    else
      return false
    end
  end

  def self.search_both(input, category)
    # TODO: メソッド間でActive Relationを受け渡す方法を探す。
    words = input.split(/ |　/)
    books_title = Book.all.includes(:category).includes(:location).eager_load(:tags)
    books_tag = Book.all.includes(:category).includes(:location).eager_load(:tags)
    words.each do |word|
      books_title = books_title.where('title like ?', '%' + word + '%')
    end
    words.each do |word|
      books_tag = books_tag.where('tags.name like ?', '%' + word + '%')
    end
    merged = books_title.or(books_tag).order('books.created_at DESC')
    if category
      books = merged.where(category_id: category.to_i).uniq
    else
      books = merged.uniq
    end
    return books
  end

  def self.search(input, category)
    words = input.split(/ |　/)
    books = Book.all.includes(:category).includes(:location)
    words.each do |word|
      books = books.where('title like ?', '%' + word + '%').order('books.created_at DESC')
    end
    if category
      books = books.where(category_id: category.to_i)
    end
    return books
  end

  def self.search_tag(input, category)
    words = input.split(/ |　/)
    books = Book.all.includes(:category).includes(:location).joins(:tags)
    words.each do |word|
      books = books.where('tags.name like ?', '%' + word + '%').order('books.created_at DESC')
    end
    if category
      books = books.where(category_id: category.to_i).uniq
    else
      books = books.uniq
    end
    return books
  end

  def self.lend(book_id, user_id)
    book = Book.find(book_id)
    book.lend(user_id)
  end
end
