class TagMap < ApplicationRecord
  belongs_to :tag
  belongs_to :book

  def self.is_dubbed(book_id, tag_id)
    tag_maps = TagMap.where(book_id: book_id)
    return tag_maps.find_by(tag_id: tag_id).blank? ? false : true
  end
end
