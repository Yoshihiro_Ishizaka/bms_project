class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :department
  devise :ldap_authenticatable,
         :recoverable, :trackable, :validatable,
         :authentication_keys => [:username]
  attr_accessor :username
  validates :username, uniqueness: true

  acts_as_paranoid
  has_many :goods
  has_many :reviews
  has_many :tags
  has_many :histories
  has_many :reservations

  def name
    read_attribute(:name) || self['username']
  
  end
  def name
    if self.deleted_at.blank?
      return read_attribute(:name) || self['username']
    else
      return self['username'] + "【削除済】"
    end
  end

  def can_borrow?
    history_count = History.where(user_id: self.id).where(is_returned: 0).count
    reservation_count = Reservation.where(user_id: self.id).count
    if (history_count + reservation_count) >= Environment.first.max_lend_count
      return false
    else
      return true
    end
  end

  # ユーザー名で検索
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if username = conditions.delete(:username)
      where(conditions).where(username: username).first
    else
      where(conditions).first
    end
  end


  # 登録時に email を不要にする
  def email_required?
    false
  end

  def email_changed?
    false
  end

  def will_save_change_to_email?
    false
  end

end
