class Review < ApplicationRecord
  belongs_to :user
  belongs_to :book
  has_many :goods

  extend OrderAsSpecified

  def check_good(user_id)
    return !(Good.where(review_id: self.id, user_id: user_id).blank?)
  end

  def count_good
    return Good.where(review_id: self.id).count
  end

  def toggle_good(user_id)
    Good.toggle_good(self.id, user_id)
  end
end
