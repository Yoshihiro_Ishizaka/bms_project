class UsersController < ApplicationController
  def show
    @is_owner = params[:id].to_i == current_user.id.to_i
    @user = User.find_by_id(params[:id])
    unless @user
      flash[:alert] = "指定IDのユーザーは存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    if params[:type] == "returned"
      @items = History.where(
        'user_id = ? and is_returned = 1', @user)
      @status = "返却済み"
    elsif params[:type] == "reserved"
      @items = Reservation.where('user_id = ?', @user)
                          .where(available_at: nil)
      @status = "予約中"
    elsif params[:type] == "ready"
      @items = Reservation.where('user_id = ?', @user)
                          .where.not(available_at: nil)
      @status = "取り置き中"
    else
      @items = History.where(
        'user_id = ? and is_returned = 0', @user)
      @status = "貸出中"
    end
    @items = @items.page(params[:page]).order('created_at DESC')
  end

  def update
    current_user.name = params[:name]
    current_user.chatwork_id = params[:chatwork_id]
    # TODO: バリデーション
    if current_user.save
      flash[:notice] = "ユーザー情報を編集しました。"
    else
      flash[:alert] = "入力された値が不正です。"
    end
    redirect_to('/users/edit')
  end

  def show_reviews
    @user = User.find_by_id(params[:id])
    unless @user
      flash[:alert] = "指定IDのユーザーは存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    @is_good = false
    @is_new = false
    if params[:mode] == 'good'
      @review_order = Review.joins(:goods).group('id').order('count_id desc').count('id')
      review_ids = []
      @review_order.each do |k, v|
        review_ids.push(k)
      end
      @reviews = Review.where(user_id: @user.id).page(params[:page]).order_as_specified(id: review_ids)
      @is_good = true
    else
      @reviews = Review.where(user_id: @user.id)
      @reviews = @reviews.page(params[:page]).order('updated_at DESC')
      @is_new = true
    end
  end

  def show_histories
    @histories = History.where(user_id: current_user.id)
  end

  def ranking
    if params['date']
      unless params['date']['from'].blank? && params['date']['to'].blank?
        unless params['date']['from'].blank?
          @from = Time.parse(params['date']['from'])
        else
          @from = Time.parse('2018/1/1')
        end

        unless params['date']['to'].blank?
          @to = Time.parse(params['date']['to'])
        else
          @to = Time.now.end_of_day
        end
        @from_def = @from.strftime('%Y-%m-%d')
        @to_def = @to.strftime('%Y-%m-%d')
      else
        @from = Time.parse('2018/1/1')
        @to = Time.now.end_of_day
        @from_def = nil
        @to_def = nil
      end
    else
      @from = Time.parse('2018/1/1')
      @to = Time.now.end_of_day
    end

    logger.debug("TEST")
    logger.debug(params[:type])
    unless params[:type].blank?
      type = params[:type]
    else
      type = "goods"
    end

    if type == "goods"
      @type = 'いいね'
      @users_order = Review.joins(:user)
                           .joins(:goods)
                           .group('user_id')
                           .where(created_at: @from...@to)
                           .order('count_id desc')
                           .count('id')
    else
      @type = '感想文投稿'
      @users_order = Review.group('user_id')
                           .where(created_at: @from...@to)
                           .order('count_user_id desc')
                           .count('user_id')
    end
  end

end
