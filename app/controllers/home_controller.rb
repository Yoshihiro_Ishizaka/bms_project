class HomeController < ApplicationController
  include ChatworkHelper

  def index
    @reviews = Review.last(2)
    @tags = Tag.order("RANDOM()").limit(40)
  end

end
