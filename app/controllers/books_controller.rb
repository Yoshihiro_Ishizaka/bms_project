require 'open-uri'
require 'json'

class BooksController < ApplicationController
  def search
    @categories = Category.all
    @category = nil
    @is_both = false
    @is_title = false
    @is_tag_text = false
    if params["cat"]
      @category = params["cat"]["category_id"]
      @category = nil if @category == ""
    end
    if (params[:text].blank? && params[:tag].blank?) && @category.nil?
      @books = Book.includes(:category, :location).page(params[:page]).per(30).order('created_at DESC')
      @is_both = true
      return
    end
    if params[:mode] == 'both'
      @books = Book.search_both(params[:text], @category)
      @is_both = true
    elsif params[:mode] == 'title'
      @books = Book.search(params[:text], @category)
      @is_title = true
    elsif params[:mode] == 'tag_text'
      @books = Book.search_tag(params[:text], @category)
      @is_tag_text = true
    elsif params[:tag]
      @books = Book.includes(:category).includes(:location).joins(:tags).where(tags: {id: params[:tag]})
      @is_both = true
    else
      @books = Book.all.includes(:category).includes(:location)
      @is_both = true
    end
    @books = Kaminari.paginate_array(@books).page(params[:page]).per(30)
  end

  def show
    @book = Book.find_by_id(params[:id])
    unless @book
      flash[:alert] = "指定IDの図書は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    else
      case @book.availability
      when 0 then @availability = '貸出中'
      when 1 then @availability = '貸出可'
      when 2 then @availability = '予約者貸出待機中'
      end
      @permitted_user = @book.oldest_reservation_user(@book.id)
      @review_count = Review.where(isbn: @book.isbn).count
      @tag_maps = TagMap.where(book_id: params[:id])

      # 本人による貸出中確認
      current_history = History.where(book_id: params[:id]).last
      if current_history
        @borrowed_by_self = current_history.user_id == current_user.id ? true : false
      else
        @borrowed_by_self = false
      end
    end
  end

  def lend
    @book = Book.find_by_id(params[:id])
    unless @book
      flash[:alert] = "指定IDの図書は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    else
      case @book.availability
      when 0 then @availability = '貸出中'
      when 1 then @availability = '貸出可'
      when 2 then @availability = '予約者貸出待機中'
      end
      @permitted_user = @book.oldest_reservation_user(@book.id)
      @reservation_count = Reservation.where(book_id: params[:id].to_i).count
      @is_reserved = Reservation.is_reserved_by_user(current_user.id, @book.id)
    end
  end

  def execute_lend
    @book = Book.find_by_id(params[:id])

    # 貸出処理
    if @book.lend(current_user.id)
      flash[:notice] = "貸出が完了しました"
      redirect_to('/')
      return
    else
      flash[:alert] = "貸出、予約冊数の上限を超えています。"
      redirect_back(fallback_location: '/')
      return
    end
  end

  def execute_reserve
    result = Reservation.reserve(current_user.id, params[:id].to_i)
    if result[0]
      flash[:notice] = "予約が完了しました"
    else
      if result[1] == :has_borrowed
        flash[:alert] = "自分が借りている図書は予約できません。"
      else
        flash[:alert] = "貸出、予約冊数の上限を超えています。"
      end
    end
    redirect_to("/books/#{params[:id]}/lend")
  end

  def delete_reservation
    reservation = Reservation.find_by(book_id: params[:id],
                                      user_id: current_user.id)
    reservation.delete
    flash[:notice] = "予約をキャンセルしました。"
    Reservation.check_and_proceed_next_reservation(params[:id].to_i)
    redirect_to("/users/#{current_user.id}?type=reserved")
  end

  def execute_return
    book = Book.find(params[:id])
    book.execute_return
    flash[:notice] = "返却が完了しました"
    redirect_to("/users/#{current_user.id}")
  end

  def show_review
    @book = Book.find_by_id(params[:id])
    unless @book
      flash[:alert] = "指定IDの図書は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    @is_good = false
    @is_new = false
    if params[:mode] == 'good'
      @review_order = Review.joins(:goods).group('id').order('count_id desc').count('id')
      review_ids = []
      @review_order.each do |k, v|
        review_ids.push(k)
      end
      @reviews = Review.all.where(isbn: @book.isbn).page(params[:page]).order_as_specified(id: review_ids)
      @is_good = true
    else
      @reviews = Review.where(isbn: @book.isbn)
      @reviews = @reviews.page(params[:page]).order('updated_at DESC')
      @is_new = true
    end
  end

  def new_review
    @book = Book.find_by_id(params[:id])
    unless @book
      flash[:alert] = "指定IDの図書は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    existing_review = Review.find_by(user_id: current_user.id, isbn: @book.isbn)
    if !(existing_review.blank?)
      redirect_to("/reviews/#{existing_review.id}/edit")
    end
  end

  def bar_code_search
    if current_user.is_admin != 1
      flash[:alert] = "管理者のみ使用できるページです。"
      redirect_to('/')
      return
    end
  end

  def bar_code_result
    @categories = Category.all
    @locations = Location.all
    if current_user.is_admin != 1
      flash[:alert] = "管理者のみ使用できるページです。"
      redirect_to('/')
      return
    end
    if params[:has_data].nil?
      redirect_to('/')
      return
    end
  end

  def bar_code_create
    if params[:update] != nil # ISBNが被った本を取得情報で更新
      books = Book.where(isbn: params[:param][:isbn])
      books.each do |book|
        params[:param].each do |k, v|
          converted_key = Constants::HASH_CONVERT_MAP[k]
          book[converted_key] = v
        end
        book.save
      end
      flash[:notice] = "#{books.count}冊の図書をまとめて更新しました。"
    else # 普通の新規登録と、ISBNが被った本をもう一冊登録する場合
      book = Book.new
      book.availability = 1

      params[:param].each do |k, v|
        converted_key = Constants::HASH_CONVERT_MAP[k]
        book[converted_key] = v
      end
      if book.save
        flash[:notice] = "「#{params[:param][:title]}」を新規図書として登録しました。"
      else 
        flash[:alert] = "必須項目が入力されていません。"
      end
    end
    redirect_to('/books/bar-code/search')
  end

  def api_send
    isbn = params[:param][:isbn].to_s

    # 入力なしでAPIを送らせない
    if isbn.blank?
      flash[:alert] = "ISBN番号が入力されていません。"
      redirect_to('/books/bar-code/search')
      return
    end

    if isbn.match(/[^(0-9|０-９]/)
      flash[:alert] = "数字のみ入力して下さい。"
      redirect_to('/books/bar-code/search')
      return
    end

    if isbn.length == 1
      isbn = isbn + '000000000000'
    end

    if isbn.length > 13
      flash[:alert] = "13桁を超える値が入力されています。"
      redirect_to('/books/bar-code/search')
      return
    end


    isbn.tr!('０-９','0-9')
    res = open(Constants::R_API_URL_BASE + isbn, proxy: Constants::PROXY)
    status = res.status[0]
    response = res.read
    parsed_json = JSON.parse(response)

    logger.debug(parsed_json)
    has_data = !(parsed_json["Items"][0].blank?)
    logger.debug('has_data')
    logger.debug(has_data)
    books = Book.where(isbn: isbn)
    unless books.blank?
      flash[:alert] = "既に同ISBN番号の図書が#{books.count}冊登録されています。"
    end

    item = nil
    if status == '200'
      if has_data
        item = parsed_json["Items"][0]["Item"].symbolize_keys
      else
        flash[:alert] = '楽天ブックスに情報がありませんでした。'
      end
    else
      flash[:alert] = "APIサーバーとの通信でエラーが発生しました。"
    end
    redirect_to action: :bar_code_result,
                item: item,
                has_data: has_data,
                is_exist: !(books.blank?)
  end

  def add_tag
    flash[:notice] = Tag.start_add_tag(
                                  current_user.id,
                                  params[:id],
                                  params[:tag])
    redirect_to("/books/#{params[:id]}/edit_tag")
  end

  def edit_tag
    @book = Book.find_by_id(params[:id].to_i)
    unless @book
      flash[:alert] = "指定IDの図書は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    @tag_maps = TagMap.where(book_id: params[:id].to_i)
  end

  def delete_tag
    params.each do |param|
      if param[0].match(/^[0-9]+$/)
        TagMap.delete([param[0].to_i])
      end
    end
    flash[:notice] = "選択されたタグを削除しました。"
    redirect_to("/books/#{params[:id]}/edit_tag")
  end

end
