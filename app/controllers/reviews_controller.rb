class ReviewsController < ApplicationController
  extend OrderAsSpecified
  def index
    @is_good = false
    @is_new = false
    if params[:mode] == 'good'
      @review_order = Review.joins(:goods).group('id').order('count_id desc').count('id')
      review_ids = []
      @review_order.each do |k, v|
        review_ids.push(k)
      end
      @reviews = Review.all.where(id: review_ids).page(params[:page]).order_as_specified(id: review_ids)
      @is_good = true
    else
      @reviews = Review.all
      @reviews = @reviews.page(params[:page]).order('updated_at DESC')
      @is_new = true
    end
  end

  def show
    @review = Review.find_by_id(params[:id])
    unless @review
      flash[:alert] = "指定IDの感想文は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
    end
    @book = Book.with_deleted.find(@review.book_id)
    @user = User.with_deleted.find(@review.user_id)
    @is_already_good = @review.check_good(current_user.id)
  end

  def new_review
    #@book
  end

  def create
    book = Book.find(params[:book_id].to_i)
    if params[:title].blank?
      flash[:alert] = "タイトルを入力して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:title].length > 100
      flash[:alert] = "タイトルは100文字以内で記入して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:text].blank?
      flash[:alert] = "本文を入力して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:text].length > 10000
      flash[:alert] = "本文は10000文字以内で記入して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end

    Review.create(
      book_id: params[:book_id].to_i,
      user_id: current_user.id.to_i,
      isbn:    book.isbn,
      title:   params[:title],
      text:    params[:text]
    )
    flash[:notice] = '感想文を作成しました。'
    redirect_to('/reviews')
  end

  def edit
    @review = Review.find_by_id(params[:id])
    unless @review
      flash[:alert] = "指定IDの感想文は存在しないか削除済です。"
      redirect_back(fallback_location: '/')
      return
    end
    @book = Book.with_deleted.find_by_id(@review.book_id)
    if current_user.id != @review.user_id.to_i
      redirect_to("/")
    end
  end

  def update
    if params[:title].blank?
      flash[:alert] = "タイトルを入力して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:title].length > 100
      flash[:alert] = "タイトルは100文字以内で記入して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:text].blank?
      flash[:alert] = "本文を入力して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end
    if params[:text].length > 10000
      flash[:alert] = "本文は10000文字以内で記入して下さい。"
      redirect_to "/books/#{book.id}/review/new"
      return
    end

    @review = Review.find(params[:id])
    @review.update(
      title: params[:title],
      text:  params[:text]
    )
    flash[:notice] = '感想文を更新しました。'
    redirect_to('/reviews')
  end

  def toggle_good
    review = Review.find(params[:id])
    review.toggle_good(current_user.id.to_i)
    redirect_to("/reviews/#{params[:id]}")
  end
end
