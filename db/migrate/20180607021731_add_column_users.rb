class AddColumnUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :ad_auth_name, :string, limit: 45
  end
end
