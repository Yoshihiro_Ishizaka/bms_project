class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :name, :limit => 100, null:false

      t.timestamps
    end
  end
end
