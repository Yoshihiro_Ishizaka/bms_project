class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string  :title, :limit => 200, null:false
      t.string  :title_kana, :limit => 300, null:false
      t.string  :author, :limit => 100, null:false
      t.string  :author_kana, :limit => 150, null:false
      t.string  :publisher_name, :limit => 100, null:false
      t.string  :isbn, :limit => 13, null:false
      t.integer :availability, :limit => 1, default: 1
      t.references :location, default: 1, foreign_key: true
      t.references :category, default: 1, foreign_key: true
      t.string  :sub_title, :limit => 200
      t.string  :sub_title_kana, :limit => 300
      t.string  :series_name, :limit => 100
      t.string  :series_name_kana, :limit => 150
      t.string  :sub_title_kana, :limit => 300
      t.string  :sub_title_kana, :limit => 300
      t.string  :size, :limit => 20
      t.string  :caption, :limit => 300
      t.string  :published_date, :limit => 11
      t.string  :image_url, :limit => 120

      t.timestamps
    end
  end
end
