class AddRememberCreatedAt < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :remember_created_at, :timestamps
  end
end
