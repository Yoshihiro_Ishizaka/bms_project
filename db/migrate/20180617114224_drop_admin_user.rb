class DropAdminUser < ActiveRecord::Migration[5.2]
  def change
    drop_table :admin_users
    remove_column :users, :ad_auth_name
    remove_column :users, :remember_created_at
  end
end
