class CreateGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :goods do |t|
      t.references :user, null:false, index:false
      t.references :review, null:false, index:false

      t.timestamps
    end
  end
end
