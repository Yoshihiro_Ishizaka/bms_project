class ChangeHistories < ActiveRecord::Migration[5.2]
  def up
    remove_column :histories, :is_returned
    remove_column :histories, :is_checked
    add_column :histories, :is_returned, :integer, :limit => 1, :default => 0
    add_column :histories, :is_checked, :integer, :limit => 1, :default => 0
  end

  def down
    remove_column :histories, :is_returned
    remove_column :histories, :is_checked
  end
end
