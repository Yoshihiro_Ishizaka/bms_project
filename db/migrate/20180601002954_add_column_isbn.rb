class AddColumnIsbn < ActiveRecord::Migration[5.2]
  def change
    add_column :reviews, :isbn, :string, limit: 13
  end
end
