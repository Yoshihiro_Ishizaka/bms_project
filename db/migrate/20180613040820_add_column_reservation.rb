class AddColumnReservation < ActiveRecord::Migration[5.2]
  def change
    add_column :reservations, :available_at, :datetime
    add_column :environments, :reservation_term, :integer, null:false, default: 7
  end
end
