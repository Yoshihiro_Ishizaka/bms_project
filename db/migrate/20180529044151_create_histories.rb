class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.references :user, null:false, index:false
      t.references :book, null:false, index:false
      t.integer :is_returned, :limit => 1, null:false
      t.integer :is_checked, :limit => 1, null:false

      t.timestamps
    end
  end
end
