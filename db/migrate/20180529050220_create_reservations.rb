class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.references :user, null:false, index:false
      t.references :book, null:false, index:false

      t.timestamps
    end
  end
end
