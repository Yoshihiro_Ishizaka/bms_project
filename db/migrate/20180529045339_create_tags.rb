class CreateTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tags do |t|
      t.references :user, null:false, index:false
      t.string  :name, :limit => 45, null:false

      t.timestamps
    end
  end
end
