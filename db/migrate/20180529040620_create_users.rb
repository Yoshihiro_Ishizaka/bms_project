class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, :limit => 45
      t.integer :is_admin, :limit => 1, default: 0
      t.references :department, default: 1, foreign_key: true
      t.string :chatwork_id, :limit => 20

      t.timestamps
    end
  end
end
