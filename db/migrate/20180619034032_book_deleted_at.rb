class BookDeletedAt < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :deleted_at, :timestamp
  end
end
