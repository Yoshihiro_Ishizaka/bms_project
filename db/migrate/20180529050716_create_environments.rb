class CreateEnvironments < ActiveRecord::Migration[5.2]
  def change
    create_table :environments do |t|
      t.integer :max_lend_count, default: 0
      t.integer :lend_term, default: 14
      t.integer :is_require_check, :limit => 1, default: 1

      t.timestamps
    end
  end
end
