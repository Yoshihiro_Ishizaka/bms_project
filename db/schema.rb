# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_06_19_034032) do

  create_table "books", force: :cascade do |t|
    t.string "title", limit: 200, null: false
    t.string "title_kana", limit: 300, null: false
    t.string "author", limit: 100, null: false
    t.string "author_kana", limit: 150, null: false
    t.string "publisher_name", limit: 100, null: false
    t.string "isbn", limit: 13, null: false
    t.integer "availability", limit: 1, default: 1
    t.integer "location_id", default: 1
    t.integer "category_id", default: 1
    t.string "sub_title", limit: 200
    t.string "sub_title_kana", limit: 300
    t.string "series_name", limit: 100
    t.string "series_name_kana", limit: 150
    t.string "size", limit: 20
    t.string "caption", limit: 300
    t.string "published_date", limit: 11
    t.string "image_url", limit: 120
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["category_id"], name: "index_books_on_category_id"
    t.index ["location_id"], name: "index_books_on_location_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "departments", force: :cascade do |t|
    t.string "name", limit: 45, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "environments", force: :cascade do |t|
    t.integer "max_lend_count", default: 0
    t.integer "lend_term", default: 14
    t.integer "is_require_check", limit: 1, default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "reservation_term", default: 7, null: false
  end

  create_table "goods", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "review_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histories", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "book_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "is_returned", limit: 1, default: 0
    t.integer "is_checked", limit: 1, default: 0
  end

  create_table "locations", force: :cascade do |t|
    t.string "name", limit: 100, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reservations", force: :cascade do |t|
    t.integer "user_id", null: false
    t.integer "book_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "available_at"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "book_id", null: false
    t.integer "user_id", null: false
    t.string "title", limit: 100, null: false
    t.text "text", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "isbn", limit: 13
  end

  create_table "tag_maps", force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "book_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "user_id"
    t.index ["book_id"], name: "index_tag_maps_on_book_id"
    t.index ["tag_id"], name: "index_tag_maps_on_tag_id"
  end

  create_table "tags", force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "name", limit: 45, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name", limit: 45
    t.integer "is_admin", limit: 1, default: 0
    t.integer "department_id", default: 1
    t.string "chatwork_id", limit: 20
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.string "username", default: "", null: false
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "remember_created_at"
    t.index ["department_id"], name: "index_users_on_department_id"
    t.index ["username"], name: "index_users_on_username", unique: true
  end

end
