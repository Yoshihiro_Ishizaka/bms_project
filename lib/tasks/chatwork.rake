namespace :chatwork do
  desc "Chatworkへ返却期限、貸出可能等通知するタスク"
  task chatwork: :environment do
    Tasks::Urge.send_message
  end
end
