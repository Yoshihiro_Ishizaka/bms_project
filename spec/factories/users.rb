FactoryBot.define do
  factory :user do
    name "MyString"
    is_admin 1
    department_id 1
    chatwork_id "MyString"
  end
end
